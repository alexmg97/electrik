module.exports = mongoose => {
    var adminsSchema = new mongoose.Schema({
        email: {
            type: String,
            default: 'admin'
        },
        password: {
            type: String,
            default: 'admin'
        },
        firstname: {
            type: String,
            default: 'Admin'
        },
        specifics: {
            isPlatformAdmin: false,
            companyName: {
                type: String,
                default: 'None'
            },
            coordonates: {
                latitude: {
                    type: Number,
                    default: 0
                },
                longitude: {
                    type: Number,
                    default: 0
                }
            }
        },
        profileImage: {
            type: String,
            default: '/public/images/default-profile.jpg'
        },
        created_at: {
            type: Date,
            default: Date.now
        },
        updated_at: {
            type: Date,
            default: Date.now
        }
    });
    var Admins = mongoose.model('Admins', adminsSchema);
    return Admins;
};