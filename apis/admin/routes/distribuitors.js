var express = require('express');
var router = express.Router();
const crypto = require('crypto');

router.post('/addDistribuitor', (req, res, next) => {
    const Admin = req.models.adminModel;
    const data = req.body.data;
    const hash = crypto.createHmac('sha256', 'electrik');

    let firstname = data.firstname,
        email = data.email,
        password = hash.update(data.passw).digest('hex').toString(),
        specifics = {
            companyName: data.compName,
            isPlatformAdmin: false,
            coordonates: {
                latitude: data.latitude,
                longitude: data.longitude
            }
        };

    Admin({
        firstname: firstname,
        email: email,
        password: password,
        specifics: specifics
    }).save((err, data) => {
        if (err) {
            throw err;
        }
        res.status(200).json({msg: "ok"});
    });
});

module.exports = router;