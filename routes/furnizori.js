const express = require('express');
const router = express.Router();
const request = require('request');

router.get('/', function (req, res, next) {

    res.render('furnizori');
});

router.post('/getFurnizori', function (req, res, next) {
    processEvent('getDistribuitors',
        [],
        (err, response, data) => {
        data = data.data;
            res.status(200).json({data});
        });
});

function processEvent(url, data = {}, cb) {
    request(process.env.API_ADMIN + url,
        {
            json: true,
            method: "POST",
            body: {
                api_key: process.env.API_ADMIN_KEY,
                data: data,
                is_admin: true
            }
        }, cb);
}

module.exports = router;
