const express = require('express');
const router = express.Router();
const request = require('request');
const multer = require('multer');
const d3 = require('d3');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/images/');
    },
    filename: function (req, file, cb) {
        //req.body is empty...
        //How could I get the new_file_name property sent from client here?
        cb(null, file.originalname);
    }
});

var upload = multer({storage: storage});

function checkAdmin(req, res, next) {
    if (req.session.isAdmin === undefined) {
        req.session.isAdmin = false;
    }

    next();
}

router.get('/', checkAdmin, function (req, res, next) {
    const auth = req.session.isAdmin;
    if (!auth) {
        res.render('ad-login');
    } else {
        if (req.session.user === 'admin') {
            res.render('ad-index');
        } else {
            res.render('ad-index-distribuitor');
        }
    }
});

router.post('/login', function (req, res, next) {
    processEvent('login', req.body,
        (err, response, data) => {
            if (err) {
                res.status(404).json({msg: "Admin API down. Try again later"});
            } else {
                if (data.err) {
                    res.status(response.statusCode).json({msg: data.msg});
                } else {
                    req.session.user = data.data.email;
                    req.session.userData = data.data;
                    req.session.isAdmin = true;
                    res.status(response.statusCode).json({msg: 'ok'});
                }
            }
        });
});

router.post('/uploadImage',
    upload.single('photo'),
    function (req, res, next) {
        res.status(200).json({msg: "ok"});
    });

router.post('/section', function (req, res, next) {
    const page = req.body.data.toLowerCase();
    const pageNo = req.body.No ? req.body.No : 0;

    console.log(page);
    switch (page) {
        case 'produse':
            processEvent(
                'products/getProducts/' + pageNo,
                {
                    user: req.session.userData
                },
                (err, response, data) => {
                    if (err) {
                        throw err;
                    }

                    res.render('ad-produse', {
                        produse: data.produse,
                        categorii: data.categorii
                    });
                });
            break;
        case 'comenzile mele':
            processEvent('getComenzileMele',
                {
                    user: req.session
                },
                (err, response, data) => {
                if(err) {
                    throw err;
                }

                res.render('comenzile-mele', {comenzi: data});
                });
            break;
        case 'rapoarte':
            res.render('ad-rapoarte');
            break;
        case 'contul meu':
            res.render('detalii-cont', {cont: req.session.user.account});
            break;
        case 'notifica':
            createNotification();
            res.status(200).json('no template needed');
            break;
        case 'categorii':
            processEvent('products/getCategories',
                [],
                (err, response, data) => {
                    res.render('ad-categorii', {categorii: data});
                });
            break;
        case 'clienti':
            processEvent('getClients',
                [],
                (err, response, data) => {
                    res.render('ad-clienti', {clienti: data.data});
                });
            break;
        case 'comenzi':
            processEvent('getOrderds',
                [],
                (err, response, data) => {
                    res.render('ad-comenzi', {comenzi: data.data});
                });
            break;
        case 'distribuitori':
            processEvent('getDistribuitors',
                [],
                (err, response, data) => {
                    res.render('ad-distribuitori', {distribuitori: data.data});
                });
            break;
        default:
            res.status(201).json('no template needed');
            break;
    }
});

router.post('/scriptsManager', function (req, res, next) {
    let page = req.body.data.toLowerCase();

    switch (page) {
        case 'rapoarte':
            res.json([
                {value: 'ad-rapoarte.js'}
            ]);
            break;
        case 'produse':
            res.json([
                {value: 'product-info.js'}
            ]);
            break;
        case 'distribuitori':
            res.json([
                {value: 'ad-distribuitori.js'}
            ]);
            break;
        case 'contul-meu':
            res.json([
                {value: 'notificari.js'}
            ]);
            break;
        case 'categorii':
            res.json([
                {
                    value: 'ad-categorii.js'
                }
            ]);
            break;
        default:
            res.status(201).json({msg: "no script needed"});
            break;
    }
});

router.post('/getCategories', (req, res, next) => {
    processEvent(
        'getCategories',
        [],
        (err, response, data) => {
            res.status(200).json({data: data});
        });
});

router.post('/addCategory', (req, res, next) => {
    let data = req.body;

    processEvent('products/categoryAdd', data, (err, response, data) => {
        res.json(data);
    });
});

router.post('/addDistribuitor', (req, res, next) => {
    let data = req.body;

    processEvent('distribuitors/addDistribuitor',
        data,
        (err, response, data) => {
            res.status(200).json({msg: "ok"});
        });
});

router.post('/addProduct', function (req, res, next) {
    request(process.env.API_ADMIN + 'products/addProduct', {
        json: true,
        method: "POST",
        body: {
            api_key: process.env.API_ADMIN_KEY,
            data: req.body,
            user: req.session.userData
        }
    }, (err, response, data) => {
        if (err) {
            res.status(404).json({msg: "Admin API down. Try again later"});
        } else {
            if (data.err) {
                res.status(response.statusCode).json({msg: data.msg});
            } else {
                processEvent(
                    'notifications/product',
                    {
                        user: req.session,
                        product: req.body
                    },
                    (err, response, data) => {
                        if (err) {
                            throw err;
                        }

                        res.status(response.statusCode).json({msg: 'ok'});
                    }
                );
            }
        }
    });
});

router.post('/addNotif', function (req, res, next) {
    request(process.env.API_ADMIN + 'notifications/add', {
        json: true,
        method: "POST",
        body: {
            api_key: process.env.API_ADMIN_KEY,
            data: req.body
        }
    }, (err, response, data) => {
        if (err) {
            res.status(404).json({msg: "Admin API down. Try again later"});
        } else {
            if (data.err) {
                res.status(response.statusCode).json({msg: data.msg});
            } else {
                req.session.user = 'admin';
                req.session.isAdmin = true;
                res.status(response.statusCode).json({msg: 'ok'});
            }
        }
    });
});

function createNotification() {
    request(process.env.API_ADMIN + 'notifications/add', {
        json: true,
        method: "POST",
        body: {
            api_key: process.env.API_ADMIN_KEY
        }
    }, (err, response, data) => {
        // if (err) {
        //     res.status(404).json({msg: "Admin API down. Try again later"});
        // }

    });
}

function processEvent(url, data = {}, cb) {
    request(process.env.API_ADMIN + url,
        {
            json: true,
            method: "POST",
            body: {
                api_key: process.env.API_ADMIN_KEY,
                data: data,
                is_admin: true
            }
        }, cb);
}

router.post('/getRapoarte', function (req, res, next) {
    processEvent('getRapoarte',
        [],
        (err, response, data) => {
            if (err) {
                throw err;
            }

            res.status(200).json(data);

            // res.status(response.statusCode).json({msg: 'ok'});
        }
    )
});

module.exports = router;
